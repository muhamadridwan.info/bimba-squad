<!-- First, extends to the CRUDBooster Layout -->
@extends('crudbooster::admin_template')
@section('content')
  <!-- Your html goes here -->
  <div class="callout callout-danger">
    Bayar memakai voucher = bayar pakai deposit, maka voucher harus di reedem dulu di menu voucher humas
  </div>
  <div class='panel panel-default'>
    <div class='panel-heading'>Pembayaran SPP</div>
    <div class='panel-body' style="padding:20px 0px 0px 0px">
      <form method='post' action="/admin/bayar-spp">
        <input name="murid_id" type="hidden" value="{{$murid->id}}"></input>
        <input name="gol_id" type="hidden" value="{{$gol->id}}"></input>
        <input name="produk_id" type="hidden" value="{{$gol->bayar_id}}"></input>

        {{ csrf_field() }}
        <div class="box-body" id="parent-form-area">
          <div class="form-group header-group-0 " id="form-group-nim" style="">
            <label class="control-label col-sm-2">NIM
            </label>
            <div class="col-sm-10">
                <input type="text" title="NIM" required="" class="form-control" name="nim" id="nim" value="{{$murid->nim}}" readonly="">
                <div class="text-danger"></div>
                <p class="help-block"></p>
            </div>
          </div>
          <div class="form-group header-group-0 " id="form-group-nama" style="">
            <label class="control-label col-sm-2">Nama Murid
            </label>
            <div class="col-sm-10">
                <input type="text" title="Nama Murid" required="" class="form-control" name="nama" id="nama" value="{{$murid->nama_murid}}" readonly="">
                <div class="text-danger"></div>
                <p class="help-block"></p>
            </div>
          </div>
          <div class="form-group header-group-0 " id="form-group-golongan" style="">
            <label class="control-label col-sm-2">Golongan
            </label>
            <div class="col-sm-10">
                <input type="text" title="Golongan" required="" class="form-control" name="golongan" id="golongan" value="{{$gol->nama}}" readonly="">
                <div class="text-danger"></div>
                <p class="help-block"></p>
            </div>
          </div>
          <div class="form-group header-group-0 " id="form-group-saldo" style="">
            <label class="control-label col-sm-2">Saldo Deposit
            </label>
            <div class="col-sm-10">
                <input type="text" title="Saldo Deposit" required="" class="form-control" name="saldo" id="saldo" value="{{$murid->saldo_deposit}}" readonly="">
                <div class="text-danger"></div>
                <p class="help-block"></p>
            </div>
          </div>
          <div class="form-group header-group-0 " id="form-group-harga" style="">
            <label class="control-label col-sm-2">Nominal SPP
            </label>
            <div class="col-sm-10">
                <input type="number" title="Nominal SPP" required=""
                class="form-control"
                readonly=""
                name="nominal" id="nominal" value="{{$gol->harga_dasar}}">
                <div class="text-danger"></div>
                <p class="help-block"></p>
            </div>
          </div>

          <div class="form-group header-group-0 " id="form-group-bydeposit" style="">
            <label class="control-label col-sm-2">Bayar Pakai Deposit
              <span class="text-danger" title="This field is required">*</span>
            </label>
            <div class="col-sm-10">
                <input type="number" title="Bayar Pakai Deposit" required=""
                max="{{$murid->saldo_deposit}}" class="form-control"
                min="0"
                name="bydeposit" id="bydeposit"
                value="{{$murid->saldo_deposit}}">
                <div class="text-danger"></div>
                <p class="help-block"></p>
            </div>
          </div>
          <div class="form-group header-group-0 " id="form-group-bytunai" style="">
            <label class="control-label col-sm-2">Bayar Tunai
              <span class="text-danger" title="This field is required">*</span>
            </label>
            <div class="col-sm-10">
                <input type="number" title="Bayar Tunai" required=""
                class="form-control"
                value="{{$gol->harga_dasar - $murid->saldo_deposit}}"
                name="bytunai" id="bytunai">
                <div class="text-danger"></div>
                <p class="help-block"></p>
            </div>
          </div>
          <div class="form-group header-group-0 " id="form-group-tgl-bukti" style="">
            <label class="control-label col-sm-2">Tanggal Kwitansi
                  <span class="text-danger" title="This field is required">*</span>
            </label>
            <div class="col-sm-10">
                <input type="date" title="Tanggal Kwitansi" required="" class="form-control" name="tgl_bukti" id="tgl_bukti">
                <div class="text-danger"></div>
                <p class="help-block"></p>
            </div>
          </div>

          <div class="form-group header-group-0 " id="form-group-bukti" style="">
            <label class="control-label col-sm-2">No Bukti
                  <span class="text-danger" title="This field is required">*</span>
            </label>
            <div class="col-sm-10">
                <input type="text" title="No Bukti" required="" maxlength="255" class="form-control" name="no_bukti" id="no_bukti">
                <div class="text-danger"></div>
                <p class="help-block"></p>
            </div>
          </div>
          <div class="form-group header-group-0 " id="form-group-bulan" style="">
            <label class="control-label col-sm-2">SPP Bulan
            </label>
            <div class="col-sm-10">
                <select class="form-control" name="bulan">
                  <option value="1">Januari</option>
                  <option value="2">Februari</option>
                  <option value="3">Maret</option>
                  <option value="4">April</option>
                  <option value="5">Mei</option>
                  <option value="6">Juni</option>
                  <option value="7">Juli</option>
                  <option value="8">Agustus</option>
                  <option value="9">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
                <div class="text-danger"></div>
                <p class="help-block"></p>
            </div>
          </div>
          <div class="form-group header-group-0 " id="form-group-ket" style="">
            <label class="control-label col-sm-2">Keterangan
            </label>
            <div class="col-sm-10">
                <input type="text" title="Keterangan" maxlength="255" class="form-control" name="ket" id="ket">
                <div class="text-danger"></div>
                <p class="help-block"></p>
            </div>
          </div>

        </div>
        <div class="box-footer" style="background: #F5F5F5">
          <div class="form-group">
                <label class="control-label col-sm-2"></label>
                <div class="col-sm-10">
                    <a href="/admin/monitoring-spp" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> Back
                    </a>
                    <input type="submit" name="submit" value="Save" class="btn btn-success">
                </div>
            </div>
        </div>
      </form>
    </div>
  </div>
@endsection
