<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminOrderController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = true;
			$this->button_edit = true;
			$this->button_delete = true;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = true;
			$this->table = "orders";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"No Bukti","name"=>"no_bukti"];
			$this->col[] = ["label"=>"Murid","name"=>"murid_id","join"=>"murid,nama_murid"];
			$this->col[] = ["label"=>"Tanggal Transaksi","name"=>"tanggal_transaksi"];
			$this->col[] = ["label"=>"Untuk Bulan","name"=>"pembayaran_bulan","join"=>"bulan,nama"];
			$this->col[] = ["label"=>"Total","name"=>"total"];
			$this->col[] = ["label"=>"Keterangan","name"=>"keterangan"];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Tanggal Transaksi','name'=>'tanggal_transaksi','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'No Bukti','name'=>'no_bukti','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Murid','name'=>'murid_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'murid,nama_murid'];
			$this->form[] = ['label'=>'Pembayaran Bulan','name'=>'pembayaran_bulan','type'=>'select2','validation'=>'required','width'=>'col-sm-9','datatable'=>'bulan,nama,id'];
			// $this->form[] = ['label'=>'Tanggal Input','name'=>'tanggal_input','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];

			// $this->form[] = ['label'=>'Murid','name'=>'murid_id','type'=>'datamodal','datamodal_table'=>'murid','datamodal_where'=>'','datamodal_columns'=>'nim,nama_murid,saldo_deposit','datamodal_columns_alias'=>'NIM,Nama Murid, Saldo Deposit','required'=>true,'datamodal_select_to'=>'saldo_deposit:saldo_deposit_awal'];


			$columns[] = ['label'=>'Produk','name'=>'produk_id','type'=>'datamodal','datamodal_table'=>'produk','datamodal_columns'=>'nama,harga','datamodal_select_to'=>'harga:harga','datamodal_where'=>'','datamodal_size'=>'small'];
			$columns[] = ['label'=>'Harga','name'=>'harga','type'=>'number','required'=>true];
			// $columns[] = ['label'=>'Voucher Diskon','name'=>'voucher_id','type'=>'select2','required'=>true, 'datatable'=>'voucher,keterangan'];
			// $columns[] = ['label'=>'Voucher Diskon','name'=>'voucher_id','type'=>'child','validation'=>'required|integer|min:0','datatable'=>'voucher,keterangan'];

			$this->form[] = ['label'=>'Detail Item','name'=>'order_detail','type'=>'child',
'columns'=>$columns,'table'=>'order_detail', 'foreign_key'=>'order_id'
		];
			$this->form[] = ['label'=>'Bayar Dengan Tunai','name'=>'by_tunai','type'=>'number','validation'=>'required','width'=>'col-sm-10', 'readonly'=>true];
			$this->form[] = ['label'=>'Bayar Dengan Deposit','name'=>'by_deposit','type'=>'number','validation'=>'required','width'=>'col-sm-10', 'readonly'=>true];
			$this->form[] = ['label'=>'Total','name'=>'total','type'=>'number','validation'=>'required','width'=>'col-sm-10', 'readonly'=>true];

			// $this->form[] = ['label'=>'Bayar Pakai Saldo','name'=>'bayar_pakai_saldo','type'=>'number','validation'=>'required','width'=>'col-sm-10', 'readonly'=>false, 'value'=>'0'];

			// $this->form[] = ['label'=>'Bayar Pakai Tunai','name'=>'bayar_pakai_tunai','type'=>'number','validation'=>'required','width'=>'col-sm-10', 'readonly'=>true];

			$this->form[] = ['label'=>'Keterangan','name'=>'keterangan','type'=>'text', 'width'=>'col-sm-10'];

			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"No Bukti","name"=>"no_bukti","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Murid","name"=>"murid_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"murid,nama_murid"];
			//$this->form[] = ["label"=>"Tanggal Input","name"=>"tanggal_input","type"=>"date","required"=>TRUE,"validation"=>"required|date"];
			//$this->form[] = ["label"=>"Tanggal Transaksi","name"=>"tanggal_transaksi","type"=>"date","required"=>TRUE,"validation"=>"required|date"];
			# OLD END FORM

			/*
	        | ----------------------------------------------------------------------
	        | Sub Module
	        | ----------------------------------------------------------------------
			| @label          = Label of action
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        |
	        */
	        $this->sub_module = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        |
	        */
	        $this->addaction = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Button Selected
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button
	        | Then about the action, you should code at actionButtonSelected method
	        |
	        */
	        $this->button_selected = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------
	        | @message = Text of message
	        | @type    = warning,success,danger,info
	        |
	        */
	        $this->alert        = array();
					$this->alert[] = ['message'=>'Untuk membayar SPP, Gunakan Menu "Monitor Pembayaran SPP" -> "Bayar". Menu ini khusus untuk Pendaftaran, Deposit, dan Penjualan lain-lain.','type'=>'danger'];


	        /*
	        | ----------------------------------------------------------------------
	        | Add more button to header button
	        | ----------------------------------------------------------------------
	        | @label = Name of button
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        |
	        */
	        $this->index_button = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	        |
	        */
	        $this->table_row_color = array();


	        /*
	        | ----------------------------------------------------------------------
	        | You may use this bellow array to add statistic at dashboard
	        | ----------------------------------------------------------------------
	        | @label, @count, @icon, @color
	        |
	        */
	        $this->index_statistic = array();
					$this->index_statistic[] = ['label'=>'Pembayaran SPP','count'=>$this->getTotalSppNew(),'color'=>'red'];
					// $this->index_statistic[] = ['label'=>'Deposit SPP','count'=>$this->getDepositMasuk(),'color'=>'yellow'];
					$this->index_statistic[] = ['label'=>'Pencairan Voucher','count'=>$this->getPenggunaanVoucher(),'color'=>'yellow'];
					// $this->index_statistic[] = ['label'=>'SPP Menggunakan Tunai','count'=>$this->getPenggunaanDeposit(),'color'=>'yellow'];

					// $this->index_statistic[] = ['label'=>'SPP Menggunakan Deposit','count'=>$this->getPenggunaanDeposit(),'color'=>'green'];
					// $this->index_statistic[] = ['label'=>'Total SPP','count'=>$this->getPenjualanSpp(),'color'=>'green'];

          $this->index_statistic[] = ['label'=>'Pendaftaran','count'=>$this->getPenjualanDaftar(),'color'=>'primary'];
          $this->index_statistic[] = ['label'=>'Lain-lain','count'=>$this->getPenjualanLain(),'color'=>'primary'];

					// $this->index_statistic[] = ['label'=>'SPP + Deposit SPP + Voucher (-1)','count'=>$this->getTotalSppBulanLalu(),'color'=>'red'];
					// $this->index_statistic[] = ['label'=>'Penggunaan Voucher (-1)','count'=>$this->getPenggunaanVoucherBulanLalu(),'color'=>'aqua'];
					// $this->index_statistic[] = ['label'=>'Penggunaan Deposit (-1)','count'=>$this->getPenggunaanDepositBulanLalu(),'color'=>'aqua'];
					// $this->index_statistic[] = ['label'=>'SPP (-1)','count'=>$this->getPenjualanSppBulanLalu(),'color'=>'aqua'];
					// $this->index_statistic[] = ['label'=>'Deposit Masuk (-1)','count'=>$this->getDepositMasukBulanLalu(),'color'=>'aqua'];
          // $this->index_statistic[] = ['label'=>'Pendaftaran (-1)','count'=>$this->getPenjualanDaftarBulanLalu(),'color'=>'aqua'];
          // $this->index_statistic[] = ['label'=>'Lain-lain (-1)','count'=>$this->getPenjualanLainBulanLalu(),'color'=>'aqua'];



					// $this->index_statistic[] = ['label'=>'Pemakaian Saldo Deposit','count'=>$this->getBayarPakaiDeposit(),'color'=>'primary'];
					// $this->index_statistic[] = ['label'=>'Bayar Tunai + Isi Deposit','count'=>$this->getBayarPakaiDeposit(),'color'=>'primary'];

	        /*
	        | ----------------------------------------------------------------------
	        | Add javascript at body
	        | ----------------------------------------------------------------------
	        | javascript code in the variable
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = "

	        $(function(){
	        	setInterval(function(){
		        	var total = 0;
		        	$('#table-detailitem tbody .harga').each(function(){
		        		var amount = parseInt($(this).text());
		        		total = total + amount;

		        	});
		        	$('#total').val(total);


		        	// var bayarPakaiSaldo = parseInt($('#bayar_pakai_saldo').val());
		        	// var tunai = total - bayarPakaiSaldo;

		        	// $('#bayar_pakai_tunai').val(tunai);

	        	}, 500);
	        });
	        ";


            /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code before index table
	        | ----------------------------------------------------------------------
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code after index table
	        | ----------------------------------------------------------------------
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include Javascript File
	        | ----------------------------------------------------------------------
	        | URL of your javascript each array
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add css style at body
	        | ----------------------------------------------------------------------
	        | css code in the variable
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = "
						#form-group-by_tunai{
							display: none
						}

						#form-group-by_deposit{
							display: none
						}
					";



	        /*
	        | ----------------------------------------------------------------------
	        | Include css File
	        | ----------------------------------------------------------------------
	        | URL of your css each array
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();


	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for button selected
	    | ----------------------------------------------------------------------
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here

	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate query of index result
	    | ----------------------------------------------------------------------
	    | @query = current sql query
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate row of index table html
	    | ----------------------------------------------------------------------
	    |
	    */
	    public function hook_row_index($column_index,&$column_value) {
	    	//Your code here
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before add data is execute
	    | ----------------------------------------------------------------------
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {
	        //Your code here
					$postdata['by_tunai'] = $postdata['total'];
	        $akun = DB::table('akun')->where('kode','KSU')->first();

	        DB::table('arus_kas')->insert([
	          'akun_id'=>$akun->id,
	          'tipe_trx'=>'DEBIT',
	          'saldo_awal'=>$akun->saldo,
	          'nominal'=>$postdata['by_tunai'],
	          'saldo_akhir'=>$akun->saldo+$postdata['by_tunai'],
	          'no_bukti'=>$postdata['no_bukti']
	        ]);

	        DB::table('akun')
	              ->where('id', $akun->id)
	              ->update(['saldo' => $akun->saldo+$postdata['by_tunai']]);

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after add public static function called
	    | ----------------------------------------------------------------------
	    | @id = last insert id
	    |
	    */
	    public function hook_after_add($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before update data is execute
	    | ----------------------------------------------------------------------
	    | @postdata = input post data
	    | @id       = current id
	    |
	    */
	    public function hook_before_edit(&$postdata,$id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_edit($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :)

     public function getTotalPenjualan(){
       $hasil = DB::select('select sum(total) as hasil from orders where tanggal_transaksi BETWEEN DATE_FORMAT(now(),"%Y-%m-01") and LAST_DAY(now())');
       return number_format($hasil[0]->hasil,0,',','.');
     }

     public function getPenjualanSpp(){
       $hasil = DB::select('
        SELECT
        	sum( od.harga ) AS hasil
        FROM
        	order_detail od
        	LEFT JOIN orders o ON o.id = od.order_id
        	LEFT JOIN produk p ON p.id = od.produk_id
        WHERE
        	(p.kategori = 1)
        	AND o.tanggal_transaksi BETWEEN DATE_FORMAT( now(), "%Y-%m-01" )
        	AND LAST_DAY(now())
      ');
       return number_format($hasil[0]->hasil,0,',','.');
     }

		 public function getPenjualanSppBulanLalu(){
       $hasil = DB::select('
        SELECT
        	sum( od.harga ) AS hasil
        FROM
        	order_detail od
        	LEFT JOIN orders o ON o.id = od.order_id
        	LEFT JOIN produk p ON p.id = od.produk_id
        WHERE
        	(p.kategori = 1)
        	AND o.tanggal_transaksi
					BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
					AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))
      ');
       return number_format($hasil[0]->hasil,0,',','.');
     }


     public function getPenjualanDaftar(){
       $hasil = DB::select('
        SELECT
        	sum( od.harga ) AS hasil
        FROM
        	order_detail od
        	LEFT JOIN orders o ON o.id = od.order_id
        	LEFT JOIN produk p ON p.id = od.produk_id
        WHERE
        	p.kategori = 2
        	AND o.tanggal_transaksi BETWEEN DATE_FORMAT( now(), "%Y-%m-01" )
        	AND LAST_DAY(now())
      ');
       return number_format($hasil[0]->hasil,0,',','.');
     }

		 public function getPenjualanDaftarBulanLalu(){
			 $hasil = DB::select('
				SELECT
				 sum( od.harga ) AS hasil
				FROM
				 order_detail od
				 LEFT JOIN orders o ON o.id = od.order_id
				 LEFT JOIN produk p ON p.id = od.produk_id
				WHERE
				 p.kategori = 2
				 AND o.tanggal_transaksi
				 BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
				 AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))
			');
			 return number_format($hasil[0]->hasil,0,',','.');
		 }

     public function getPenjualanLain(){
       $hasil = DB::select('
        SELECT
        	sum( od.harga ) AS hasil
        FROM
        	order_detail od
        	LEFT JOIN orders o ON o.id = od.order_id
        	LEFT JOIN produk p ON p.id = od.produk_id
        WHERE
        	p.kategori = 3
        	AND o.tanggal_transaksi BETWEEN DATE_FORMAT( now(), "%Y-%m-01" )
        	AND LAST_DAY(now())
      ');
       return number_format($hasil[0]->hasil,0,',','.');
     }

		 public function getPenjualanLainBulanLalu(){
       $hasil = DB::select('
        SELECT
        	sum( od.harga ) AS hasil
        FROM
        	order_detail od
        	LEFT JOIN orders o ON o.id = od.order_id
        	LEFT JOIN produk p ON p.id = od.produk_id
        WHERE
        	p.kategori = 3
					AND o.tanggal_transaksi
					BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
					AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))
      ');
       return number_format($hasil[0]->hasil,0,',','.');
     }

		 public function getBayarPakaiDeposit(){
			 $hasil = DB::select('
			 SELECT
			 	sum(o.by_deposit) AS hasil
			 from
			 	orders o
			 where
			 	o.tanggal_transaksi BETWEEN DATE_FORMAT( now(), "%Y-%m-01" ) AND LAST_DAY(now());
			');
			 return number_format($hasil[0]->hasil,0,',','.');
		 }

		 public function getPenggunaanVoucher(){
			 $hasil = DB::select('
			 SELECT
			 	COALESCE (sum(o.nominal),0) as hasil
			 from
			 	voucher o
			 where
			 	o.tanggal_pencairan BETWEEN DATE_FORMAT( now(), "%Y-%m-01" ) AND LAST_DAY(now());
			');
			 return number_format($hasil[0]->hasil,0,',','.');
		 }

		 public function getPenggunaanVoucherBulanLalu(){
			 $hasil = DB::select('
			 SELECT
			 	COALESCE (sum(o.nominal),0) as hasil
			 from
			 	voucher o
			 where
			 	o.tanggal_pencairan BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" ) AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH));
			');
			 return number_format($hasil[0]->hasil,0,',','.');
		 }

		 public function getTotalSpp(){
			 $total = intVal(str_replace('.', '', $this->getPenjualanSpp()))  - intVal(str_replace('.', '', $this->getBayarPakaiDeposit()));
			 // $total = 0;
			 return number_format($total,0,',','.');
		 }

		 public function getTotalSppNew(){
			 $hasil = DB::select('
				 SELECT (
				 	-- Transaksi Iuran SPP + Deposit SPP + Pencairan Voucher - Bayar Pakai Deposit
				 	SELECT
				 		sum( od.harga ) AS hasil
				 	FROM
				 		order_detail od
				 		LEFT JOIN orders o ON o.id = od.order_id
				 		LEFT JOIN produk p ON p.id = od.produk_id
				 	WHERE
				 		(p.kategori = 1 or p.kategori = 4)
				 		AND o.tanggal_transaksi BETWEEN DATE_FORMAT( now(), "%Y-%m-01" )
				 		AND LAST_DAY(now())) + (
				 	SELECT
				 		COALESCE (sum(o.nominal),0) as hasil
				 	from
				 		voucher o
				 	where
				 		o.tanggal_pencairan BETWEEN DATE_FORMAT( now(), "%Y-%m-01" ) AND LAST_DAY(now())) - (
				 	SELECT
				 		sum(o.by_deposit)
				 	from
				 		orders o
				 	where
				 		o.tanggal_transaksi BETWEEN DATE_FORMAT( now(), "%Y-%m-01" ) AND LAST_DAY(now())
				 ) as hasil;
			');
			 return number_format($hasil[0]->hasil,0,',','.');
		 }

		 public function getTotalSppBulanLalu(){
			 $hasil = DB::select('
				 SELECT (
					-- Transaksi Iuran SPP + Deposit SPP + Pencairan Voucher - Bayar Pakai Deposit
					SELECT
						sum( od.harga ) AS hasil
					FROM
						order_detail od
						LEFT JOIN orders o ON o.id = od.order_id
						LEFT JOIN produk p ON p.id = od.produk_id
					WHERE
						(p.kategori = 1 or p.kategori = 4)
						AND o.tanggal_transaksi
						BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
						AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))) + (
					SELECT
						COALESCE (sum(o.nominal),0) as hasil
					from
						voucher o
					where
						o.tanggal_pencairan
						BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
						AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))) - (
					SELECT
						COALESCE (sum(o.by_deposit),0)
					from
						orders o
					where
						o.tanggal_transaksi
						BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
						AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))
				 ) as hasil;
			');
			 return number_format($hasil[0]->hasil,0,',','.');
		 }


		 public function getPenggunaanDeposit(){
			 $hasil = DB::select('
				 SELECT (
				 		SELECT
				 		sum(o.by_deposit)
				 	from
				 		orders o
				 	where
				 		o.tanggal_transaksi
						BETWEEN DATE_FORMAT( now(), "%Y-%m-01" ) AND LAST_DAY(now()))
						-(
				 	SELECT
				 		COALESCE (sum(o.nominal),0) as hasil
				 	from
				 		voucher o
				 	where
				 		o.tanggal_pencairan BETWEEN DATE_FORMAT( now(), "%Y-%m-01" ) AND LAST_DAY(now())
				 ) as hasil;
			');
			 return number_format($hasil[0]->hasil,0,',','.');
		 }

		 public function getPenggunaanDepositBulanLalu(){
			 $hasil = DB::select('
				 SELECT (
				 		SELECT
				 		sum(o.by_deposit)
				 	from
				 		orders o
				 	where
				 		o.tanggal_transaksi
						BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
						AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))) - (
				 	SELECT
				 		COALESCE (sum(o.nominal),0) as hasil
				 	from
				 		voucher o
				 	where
				 		o.tanggal_pencairan
						BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
						AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))
					) as hasil;
			');
			 return number_format($hasil[0]->hasil,0,',','.');
		 }

		 public function getDepositMasuk(){
		 	$hasil = DB::select('
					SELECT
			 		 COALESCE (sum( od.harga ),0) AS hasil
			 	 FROM
			 		 order_detail od
			 		 LEFT JOIN orders o ON o.id = od.order_id
			 		 LEFT JOIN produk p ON p.id = od.produk_id
			 	 WHERE
			 		 (p.kategori = 4)
			 		 AND o.tanggal_transaksi BETWEEN DATE_FORMAT( now(), "%Y-%m-01" )
			 		 AND LAST_DAY(now());
		  ');
		 	return number_format($hasil[0]->hasil,0,',','.');
		 }

		 public function getDepositMasukBulanLalu(){
		 	$hasil = DB::select('
					SELECT
			 		 COALESCE (sum( od.harga ),0) AS hasil
			 	 FROM
			 		 order_detail od
			 		 LEFT JOIN orders o ON o.id = od.order_id
			 		 LEFT JOIN produk p ON p.id = od.produk_id
			 	 WHERE
			 		 (p.kategori = 4)
			 		 AND o.tanggal_transaksi
					 BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
 					 AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))
		  ');
		 	return number_format($hasil[0]->hasil,0,',','.');
		 }

	}
