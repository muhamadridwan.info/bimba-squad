<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminMonitoringSppController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "60";
			$this->orderby = "id,asc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = false;
			$this->button_delete = false;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = true;
			$this->button_import = false;
			$this->button_export = true;
			$this->table = "dummy_table";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nim","name"=>"vw_montoring_pembayaran_spp.nim"];
			$this->col[] = ["label"=>"Nama Murid","name"=>"vw_montoring_pembayaran_spp.nama_murid"];
			$this->col[] = ["label"=>"Golongan","name"=>"vw_montoring_pembayaran_spp.golongan"];
			$this->col[] = ["label"=>"Wali Kelas","name"=>"vw_montoring_pembayaran_spp.wali_kelas"];
			$this->col[] = ["label"=>"Nominal SPP","name"=>"vw_montoring_pembayaran_spp.biaya_spp"];
			// $this->col[] = ["label"=>"Tansaksi Sebelumnya","name"=>"vw_montoring_pembayaran_spp.transaksi_terakhir"];
			// $this->col[] = ["label"=>"Untuk Bulan","name"=>"vw_montoring_pembayaran_spp.pembayaran_bulan"];
			// $this->col[] = ["label"=>"Belum Bayar (Bulan)","name"=>"vw_montoring_pembayaran_spp.belum_bayar_bulan"];
			// $this->col[] = ["label"=>"Belum Bayar (Nominal)","name"=>"vw_montoring_pembayaran_spp.belum_bayar_nominal"];
			// $this->col[] = ["label"=>"Status","name"=>"vw_montoring_pembayaran_spp.pembayaran_bulan","callback_php"=>'$this->ubahWarna($row->pembayaran_bulan)'];
			$this->col[] = ["label"=>"Tanggal Bayar","name"=>"vw_montoring_pembayaran_spp.bayar"];

			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required','width'=>'col-sm-9'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			# OLD END FORM

			/*
	        | ----------------------------------------------------------------------
	        | Sub Module
	        | ----------------------------------------------------------------------
			| @label          = Label of action
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        |
	        */
	        $this->sub_module = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        |
	        */
					$this->addaction = array();
					$this->addaction[] = ['label'=>'Bayar','url'=>CRUDBooster::adminPath('bayar-spp/[nim]'),'color'=>'success',
					'showIf'=>"[bayar] == null"
				];


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Button Selected
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button
	        | Then about the action, you should code at actionButtonSelected method
	        |
	        */
	        $this->button_selected = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------
	        | @message = Text of message
	        | @type    = warning,success,danger,info
	        |
	        */
	        $this->alert        = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add more button to header button
	        | ----------------------------------------------------------------------
	        | @label = Name of button
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        |
	        */
	        $this->index_button = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	        |
	        */
	        $this->table_row_color = array();
	        $this->table_row_color[] = ['condition'=>"[bayar] == null","color"=>"danger"];
	        // $this->table_row_color[] = ['condition'=>$this->ubahWarna('[pembayaran_bulan]') != true,"color"=>"danger"];
					// $this->table_row_color[] = $this->ubahWarna();


	        /*
	        | ----------------------------------------------------------------------
	        | You may use this bellow array to add statistic at dashboard
	        | ----------------------------------------------------------------------
	        | @label, @count, @icon, @color
	        |
	        */
	        $this->index_statistic = array();
					$this->index_statistic[] = ['label'=>'SPP KU','count'=>$this->getTotalSppNew(),'color'=>'red'];
					$this->index_statistic[] = ['label'=>'Faktor Kali KU','count'=>$this->getFactorKaliKU(),'color'=>'aqua'];
					// $this->index_statistic[] = ['label'=>'Murid Aktif','count'=>$this->getFactorKali(),'color'=>'aqua'];
					$this->index_statistic[] = ['label'=>'Progressive KU','count'=>$this->getProgressiveKU(),'color'=>'aqua'];
					$this->index_statistic[] = ['label'=>'Target Selanjutnya','count'=>$this->getTargetKUSelanjutnya(),'color'=>'green'];
					$this->index_statistic[] = ['label'=>'SPP Mtv 1','count'=>$this->getSPPMTV1(),'color'=>'red'];
					$this->index_statistic[] = ['label'=>'Faktor Kali Mtv 1','count'=>$this->getFactorKaliMTV1(),'color'=>'aqua'];
					$this->index_statistic[] = ['label'=>'Progressive Mtv 1','count'=>$this->getProgressiveMTV1(),'color'=>'aqua'];
					$this->index_statistic[] = ['label'=>'SPP Mtv 2','count'=>$this->getSPPMTV2(),'color'=>'red'];
					$this->index_statistic[] = ['label'=>'Faktor Kali Mtv 2','count'=>$this->getFactorKaliMTV2(),'color'=>'aqua'];
					$this->index_statistic[] = ['label'=>'Progressive Mtv 2','count'=>$this->getProgressiveMTV2(),'color'=>'aqua'];

	        /*
	        | ----------------------------------------------------------------------
	        | Add javascript at body
	        | ----------------------------------------------------------------------
	        | javascript code in the variable
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code before index table
	        | ----------------------------------------------------------------------
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code after index table
	        | ----------------------------------------------------------------------
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include Javascript File
	        | ----------------------------------------------------------------------
	        | URL of your javascript each array
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add css style at body
	        | ----------------------------------------------------------------------
	        | css code in the variable
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;



	        /*
	        | ----------------------------------------------------------------------
	        | Include css File
	        | ----------------------------------------------------------------------
	        | URL of your css each array
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();


	    }

			public function ubahWarna($x){
				// dd($x);
				if ($x != 'Juni') {
					return 'BELUM BAYAR';
				}else{
					return 'SUDAH BAYAR';
				}
			}


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for button selected
	    | ----------------------------------------------------------------------
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here

	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate query of index result
	    | ----------------------------------------------------------------------
	    | @query = current sql query
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	        $query->rightJoin('vw_montoring_pembayaran_spp', 'vw_montoring_pembayaran_spp.id', '=', 'dummy_table.id');
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate row of index table html
	    | ----------------------------------------------------------------------
	    |
	    */
	    public function hook_row_index($column_index,&$column_value) {
	    	//Your code here
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before add data is execute
	    | ----------------------------------------------------------------------
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after add public static function called
	    | ----------------------------------------------------------------------
	    | @id = last insert id
	    |
	    */
	    public function hook_after_add($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before update data is execute
	    | ----------------------------------------------------------------------
	    | @postdata = input post data
	    | @id       = current id
	    |
	    */
	    public function hook_before_edit(&$postdata,$id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_edit($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :)
			public function getTotalSppNew(){
 			 $hasil = DB::select('
 				 SELECT (
 				 	-- Transaksi Iuran SPP + Deposit SPP + Pencairan Voucher - Bayar Pakai Deposit
 				 	SELECT
 				 		sum( od.harga ) AS hasil
 				 	FROM
 				 		order_detail od
 				 		LEFT JOIN orders o ON o.id = od.order_id
 				 		LEFT JOIN produk p ON p.id = od.produk_id
 				 	WHERE
 				 		(p.kategori = 1 or p.kategori = 4)
 				 		AND o.tanggal_transaksi BETWEEN DATE_FORMAT( now(), "%Y-%m-01" )
 				 		AND LAST_DAY(now())) + (
 				 	SELECT
 				 		COALESCE (sum(o.nominal),0) as hasil
 				 	from
 				 		voucher o
 				 	where
 				 		o.tanggal_pencairan BETWEEN DATE_FORMAT( now(), "%Y-%m-01" ) AND LAST_DAY(now())) - (
 				 	SELECT
 				 		sum(o.by_deposit)
 				 	from
 				 		orders o
 				 	where
 				 		o.tanggal_transaksi BETWEEN DATE_FORMAT( now(), "%Y-%m-01" ) AND LAST_DAY(now())
 				 ) as hasil;
 			');
 			 return number_format($hasil[0]->hasil,0,',','.');
 		 }

		 public function getTotalSppBulanLalu(){
			 $hasil = DB::select('
				 SELECT (
					-- Transaksi Iuran SPP + Deposit SPP + Pencairan Voucher - Bayar Pakai Deposit
					SELECT
						sum( od.harga ) AS hasil
					FROM
						order_detail od
						LEFT JOIN orders o ON o.id = od.order_id
						LEFT JOIN produk p ON p.id = od.produk_id
					WHERE
						(p.kategori = 1 or p.kategori = 4)
						AND o.tanggal_transaksi
						BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
						AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))) + (
					SELECT
						COALESCE (sum(o.nominal),0) as hasil
					from
						voucher o
					where
						o.tanggal_pencairan
						BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
						AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))) - (
					SELECT
						COALESCE (sum(o.by_deposit),0)
					from
						orders o
					where
						o.tanggal_transaksi
						BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
						AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))
				 ) as hasil;
			');
			 return number_format($hasil[0]->hasil,0,',','.');
		 }

		 public function getSPPMTV1BulanLalu(){
			 $hasil = DB::select('
				 SELECT (
					-- Transaksi Iuran SPP + Deposit SPP + Pencairan Voucher - Bayar Pakai Deposit
					SELECT
						COALESCE(sum( od.harga ),0) AS hasil
					FROM
						order_detail od
						LEFT JOIN orders o ON o.id = od.order_id
						LEFT JOIN produk p ON p.id = od.produk_id
						LEFT JOIN murid m On o.murid_id = m.id
					WHERE
						(p.kategori = 1 or p.kategori = 4)
						AND m.wali_kelas_id = 2
						AND o.tanggal_transaksi
						BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
						AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))) + (
					SELECT
						COALESCE (sum(v.nominal),0) as hasil
					from
						voucher v
						LEFT JOIN murid m2 ON v.humas_murid_id = m2.id
					where
						m2.wali_kelas_id = 2 AND
						v.tanggal_pencairan
						BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
						AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))) - (
					SELECT
						COALESCE (sum(o.by_deposit),0)
					from
						orders o
						LEFT JOIN murid m On o.murid_id = m.id
					where
						m.wali_kelas_id = 2 AND
						o.tanggal_transaksi
						BETWEEN DATE_FORMAT(DATE_ADD(NOW(),  INTERVAL -1 MONTH), "%Y-%m-01" )
						AND LAST_DAY(DATE_ADD(NOW(),  INTERVAL -1 MONTH))
				 ) as hasil;
			');
			 return number_format($hasil[0]->hasil,0,',','.');
		 }

		 public function getSPPMTV1(){
			 $hasil = DB::select('
				 SELECT (
					-- Transaksi Iuran SPP + Deposit SPP + Pencairan Voucher - Bayar Pakai Deposit
					SELECT
						COALESCE(sum( od.harga ),0) AS hasil
					FROM
						order_detail od
						LEFT JOIN orders o ON o.id = od.order_id
						LEFT JOIN produk p ON p.id = od.produk_id
						LEFT JOIN murid m On o.murid_id = m.id
					WHERE
						(p.kategori = 1 or p.kategori = 4)
						AND m.wali_kelas_id = 2
						AND o.tanggal_transaksi BETWEEN DATE_FORMAT( now(), "%Y-%m-01" )
 				 		AND LAST_DAY(now())) + (
					SELECT
						COALESCE (sum(v.nominal),0) as hasil
					from
						voucher v
						LEFT JOIN murid m2 ON v.humas_murid_id = m2.id
					where
						m2.wali_kelas_id = 2 AND
						v.tanggal_pencairan
						BETWEEN DATE_FORMAT( now(), "%Y-%m-01" )
						AND LAST_DAY(now())) - (
					SELECT
						COALESCE (sum(o.by_deposit),0)
					from
						orders o
						LEFT JOIN murid m On o.murid_id = m.id
					where
						m.wali_kelas_id = 2 AND
						o.tanggal_transaksi
						BETWEEN DATE_FORMAT( now(), "%Y-%m-01" )
						AND LAST_DAY(now())
				 ) as hasil;
			');
			 return number_format($hasil[0]->hasil,0,',','.');
		 }

		 public function getSPPMTV2(){
			 $hasil = DB::select('
				 SELECT (
					-- Transaksi Iuran SPP + Deposit SPP + Pencairan Voucher - Bayar Pakai Deposit
					SELECT
						COALESCE(sum( od.harga ),0) AS hasil
					FROM
						order_detail od
						LEFT JOIN orders o ON o.id = od.order_id
						LEFT JOIN produk p ON p.id = od.produk_id
						LEFT JOIN murid m On o.murid_id = m.id
					WHERE
						(p.kategori = 1 or p.kategori = 4)
						AND m.wali_kelas_id = 4
						AND o.tanggal_transaksi BETWEEN DATE_FORMAT( now(), "%Y-%m-01" )
 				 		AND LAST_DAY(now())) + (
					SELECT
						COALESCE (sum(v.nominal),0) as hasil
					from
						voucher v
						LEFT JOIN murid m2 ON v.humas_murid_id = m2.id
					where
						m2.wali_kelas_id = 4 AND
						v.tanggal_pencairan
						BETWEEN DATE_FORMAT( now(), "%Y-%m-01" )
						AND LAST_DAY(now())) - (
					SELECT
						COALESCE (sum(o.by_deposit),0)
					from
						orders o
						LEFT JOIN murid m On o.murid_id = m.id
					where
						m.wali_kelas_id = 4 AND
						o.tanggal_transaksi
						BETWEEN DATE_FORMAT( now(), "%Y-%m-01" )
						AND LAST_DAY(now())
				 ) as hasil;
			');
			 return number_format($hasil[0]->hasil,0,',','.');
		 }

		 public function getFactorKaliKU(){
			 $total = intVal(str_replace('.', '', $this->getTotalSppNew()));
			 $x = floor($total / 200000 * 1.17);
			 return number_format($x,0,',','.');
		 }

		 public function getFactorKaliMTV1(){
			 $total = intVal(str_replace('.', '', $this->getSPPMTV1()));
			 $x =  floor($total / 200000 * 1.17);
			 return number_format($x,0,',','.');
		 }

		 public function getFactorKaliMTV2(){
			 $total = intVal(str_replace('.', '', $this->getSPPMTV2()));
			 $x =  floor($total / 200000 * 1.17);
			 return number_format($x,0,',','.');
		 }


		 public function getMuridAktif(){
			 $hasil = DB::select('select count(*) as hasil from murid where status = "Aktif"');
			 return $hasil[0]->hasil;
		 }

		 public function getProgressiveKU(){
				$hasil = DB::select('
							SELECT
								pk.tarif
							from
								progressive pk
							where
								pk.sdm = "KU"
								and ('.$this->getFactorKaliKU().' >= pk.min and '.$this->getFactorKaliKU().' <= pk.max)
							LIMIT 1;
				');
			 return number_format($hasil[0]->tarif,0,',','.');
		 }

		 public function getProgressiveMTV1(){
				$hasil = DB::select('
							SELECT
								pk.tarif
							from
								progressive pk
							where
								pk.sdm = "MTV"
								and ('.$this->getFactorKaliMTV1().' >= pk.min and '.$this->getFactorKaliMTV1().' <= pk.max)
							LIMIT 1;
				');
			 return number_format($hasil[0]->tarif,0,',','.');
		 }

		 public function getProgressiveMTV2(){
				$hasil = DB::select('
							SELECT
								pk.tarif
							from
								progressive pk
							where
								pk.sdm = "MTV"
								and ('.$this->getFactorKaliMTV2().' >= pk.min and '.$this->getFactorKaliMTV2().' <= pk.max)
							LIMIT 1;
				');
			 return number_format($hasil[0]->tarif,0,',','.');
		 }

		 public function getTargetKUSelanjutnya(){
				$hasil = DB::select('
							SELECT
								pk.target_selanjutnya
							from
								progressive pk
							where
								pk.sdm = "KU"
								and ('.$this->getFactorKaliKU().' >= pk.min and '.$this->getFactorKaliKU().' <= pk.max)
							LIMIT 1;
				');

				$hasil2 = DB::select('
							SELECT
								pk.tarif
							from
								progressive pk
							where
								pk.sdm = "KU"
								and ('.$hasil[0]->target_selanjutnya.' >= pk.min and '.$hasil[0]->target_selanjutnya.' <= pk.max)
							LIMIT 1;
				');

			 return number_format($hasil2[0]->tarif,0,',','.').'/'.
			 number_format($hasil[0]->target_selanjutnya,0,',','.');
		 }


	}
