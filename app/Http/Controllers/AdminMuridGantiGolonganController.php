<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;
	use Illuminate\Support\Facades\Http;

	class AdminMuridGantiGolonganController extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "id";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = false;
			$this->button_table_action = true;
			$this->button_bulk_action = false;
			$this->button_action_style = "button_dropdown";
			$this->button_add = true;
			$this->button_edit = false;
			$this->button_delete = true;
			$this->button_detail = false;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = true;
			$this->table = "murid_ganti_golongan";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Tanggal Input","name"=>"murid_ganti_golongan.tanggal_input"];
			$this->col[] = ["label"=>"NIM","name"=>"murid.nim"];
			$this->col[] = ["label"=>"Nama Murid","name"=>"murid.nama_murid"];
			$this->col[] = ["label"=>"Sebelumnya","name"=>"gol1.nama"];
			$this->col[] = ["label"=>"Menjadi","name"=>"gol2.nama"];
			// $this->col[] = ["label"=>"Sebelumnya","name"=>"murid_ganti_golongan.gol_sebelum","join"=>"golongan,nama"];
			// $this->col[] = ["label"=>"Menjadi","name"=>"murid_ganti_golongan.gol_sesudah","join"=>"golongan,nama"];
			// $this->col[] = ["label"=>"Tanggal Efektif","name"=>"murid_ganti_golongan.tanggal_efektif"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Murid','name'=>'murid_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'murid,nama_murid'];
			$this->form[] = ['label'=>'Sebelumnya','name'=>'gol_sebelum','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'golongan,nama'];
			$this->form[] = ['label'=>'Menjadi','name'=>'gol_sesudah','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'golongan,nama'];
			// $this->form[] = ['label'=>'Tanggal Efektif','name'=>'tanggal_efektif','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ['label'=>'Murid','name'=>'murid_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'murid,nama_murid'];
			//$this->form[] = ['label'=>'Sebelumnya','name'=>'gol_sebelum','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'golongan,nama'];
			//$this->form[] = ['label'=>'Menjadi','name'=>'gol_sesudah','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'golongan,nama'];
			//$this->form[] = ['label'=>'Tanggal Efektif','name'=>'tanggal_efektif','type'=>'date','validation'=>'required|date','width'=>'col-sm-10'];
			# OLD END FORM

			/*
	        | ----------------------------------------------------------------------
	        | Sub Module
	        | ----------------------------------------------------------------------
			| @label          = Label of action
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        |
	        */
	        $this->sub_module = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        |
	        */
	        $this->addaction = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add More Button Selected
	        | ----------------------------------------------------------------------
	        | @label       = Label of action
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button
	        | Then about the action, you should code at actionButtonSelected method
	        |
	        */
	        $this->button_selected = array();


	        /*
	        | ----------------------------------------------------------------------
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------
	        | @message = Text of message
	        | @type    = warning,success,danger,info
	        |
	        */
	        $this->alert        = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add more button to header button
	        | ----------------------------------------------------------------------
	        | @label = Name of button
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        |
	        */
	        $this->index_button = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.
	        |
	        */
	        $this->table_row_color = array();


	        /*
	        | ----------------------------------------------------------------------
	        | You may use this bellow array to add statistic at dashboard
	        | ----------------------------------------------------------------------
	        | @label, @count, @icon, @color
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add javascript at body
	        | ----------------------------------------------------------------------
	        | javascript code in the variable
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code before index table
	        | ----------------------------------------------------------------------
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include HTML Code after index table
	        | ----------------------------------------------------------------------
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;



	        /*
	        | ----------------------------------------------------------------------
	        | Include Javascript File
	        | ----------------------------------------------------------------------
	        | URL of your javascript each array
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();



	        /*
	        | ----------------------------------------------------------------------
	        | Add css style at body
	        | ----------------------------------------------------------------------
	        | css code in the variable
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;



	        /*
	        | ----------------------------------------------------------------------
	        | Include css File
	        | ----------------------------------------------------------------------
	        | URL of your css each array
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();


	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for button selected
	    | ----------------------------------------------------------------------
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here

	    }


	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate query of index result
	    | ----------------------------------------------------------------------
	    | @query = current sql query
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
					$query->join('murid','murid_ganti_golongan.murid_id','=','murid.id')
								->join('golongan as gol1','gol1.id','=','murid_ganti_golongan.gol_sebelum')
								->join('golongan as gol2','gol2.id','=','murid_ganti_golongan.gol_sesudah');
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate row of index table html
	    | ----------------------------------------------------------------------
	    |
	    */
	    public function hook_row_index($column_index,&$column_value) {
	    	//Your code here
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before add data is execute
	    | ----------------------------------------------------------------------
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {
	        //Your code here

					if ($postdata['gol_sebelum']==$postdata['gol_sesudah']) {
						CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"GAGAL! Golongan sebelum dan sesudah tidak boleh sama!","danger");
					}

	        $murid = DB::table('murid')->where('id',$postdata['murid_id'])->first();
					if ($murid->golongan_id != $postdata['gol_sebelum']) {
				 			CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"GAGAL! Golongan sebelumnya tidak sesuai, silahkan coba lagi","danger");
					}

	        $golongan_sebelum = DB::table('golongan')->where('id',$postdata['gol_sebelum'])->first();
					$golongan_sesudah = DB::table('golongan')->where('id',$postdata['gol_sesudah'])->first();

					$message = 'Murid '.$murid->nama_murid.
										' berganti Golongan dari '.$golongan_sebelum->nama.' menjadi '.$golongan_sesudah->nama.'. Diinput oleh '.CRUDBooster::myName();
					$botApiKey = '1960618892:AAHFTdIv7c4louJM3-1jzV0zakQcJDPZtfY';

					$response = Http::get("https://api.telegram.org/bot".$botApiKey."/sendMessage", [
					    'chat_id' => '-1001383434128',
					    'text' => $message,
					]);
	        // DB::table('arus_kas')->insert([
	        //   'akun_id'=>$akun->id,
	        //   'tipe_trx'=>'DEBIT',
	        //   'saldo_awal'=>$akun->saldo,
	        //   'nominal'=>$postdata['by_tunai'],
	        //   'saldo_akhir'=>$akun->saldo+$postdata['by_tunai'],
	        //   'no_bukti'=>$postdata['no_bukti']
	        // ]);
					//
	        // DB::table('akun')
	        //       ->where('id', $akun->id)
	        //       ->update(['saldo' => $akun->saldo+$postdata['by_tunai']]);
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after add public static function called
	    | ----------------------------------------------------------------------
	    | @id = last insert id
	    |
	    */
	    public function hook_after_add($id) {
	        //Your code here
					$mgg = DB::table('murid_ganti_golongan')->where('id',$id)->first();
					DB::table('murid')
	              ->where('id', $mgg->murid_id)
	              ->update(['golongan_id' => $mgg->gol_sesudah]);
	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for manipulate data input before update data is execute
	    | ----------------------------------------------------------------------
	    | @postdata = input post data
	    | @id       = current id
	    |
	    */
	    public function hook_before_edit(&$postdata,$id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_edit($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /*
	    | ----------------------------------------------------------------------
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------
	    | @id       = current id
	    |
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :)


	}
