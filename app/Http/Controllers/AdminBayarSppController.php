<?php namespace App\Http\Controllers;

	use Session;
	use DB;
	use CRUDBooster;
  use Illuminate\Http\Request;
  use Illuminate\Support\Facades\Auth;

class AdminBayarSppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($muridid)
    {
        if(CRUDBooster::myPrivilegeId()==null){
          return redirect('/admin/login');
        }

        $murid = DB::table('murid')->where('nim',$muridid)->first();
        if ($murid == null) {
          return redirect('/admin/monitoring-spp');
        }

        $gol = DB::table('golongan')->where('id',$murid->golongan_id)->first();

        $data = [];
        $data['page_title'] = 'Pembayaran SPP';
        $data['murid'] = $murid;
        $data['gol'] = $gol;

        // dd($data);

        //Please use view method instead view method from laravel
        return view('custom_add_view',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      if(CRUDBooster::myPrivilegeId()==null){
        return redirect('/admin/login');
      }

      $murid_id = $request->input('murid_id');
      $bulan = $request->input('bulan');
      $bydeposit = $request->input('bydeposit');
      $bytunai = $request->input('bytunai');
      $tgl_bukti = $request->input('tgl_bukti');
      $no_bukti = $request->input('no_bukti');
      $ket = $request->input('ket');
      $gol_id = $request->input('gol_id');
      $produk_id = $request->input('produk_id');
      $saldo = $request->input('saldo');

      DB::table('pembayaran_spp')->insert([
        'no_bukti'=>$no_bukti,
        'murid_id'=>$murid_id,
        'tanggal_transaksi'=>$tgl_bukti,
        'saldo_deposit'=>$bydeposit,
        'tunai'=>$bytunai,
        'nominal'=>$bytunai+$bydeposit,
        'golongan_id'=>$gol_id
      ]);

      $order_id = DB::table('orders')->insertGetId([
        'no_bukti'=>$no_bukti,
        'murid_id'=>$murid_id,
        'tanggal_transaksi'=>$tgl_bukti,
        'total'=>$bytunai+$bydeposit,
        'keterangan'=>$ket,
        'by_tunai'=>$bytunai,
        'by_deposit'=>$bydeposit,
				'pembayaran_bulan'=>$bulan
      ]);

      DB::table('order_detail')->insert([
        'order_id'=>$order_id,
        'produk_id'=>$produk_id,
        'harga'=>$bytunai+$bydeposit
      ]);

      if ($bytunai!=null && $bytunai > 0) {
        $akun = DB::table('akun')->where('kode','KSU')->first();

        DB::table('arus_kas')->insert([
          'akun_id'=>$akun->id,
          'tipe_trx'=>'DEBIT',
          'saldo_awal'=>$akun->saldo,
          'nominal'=>$bytunai,
          'saldo_akhir'=>$akun->saldo+$bytunai,
          'no_bukti'=>$no_bukti
        ]);

        DB::table('akun')
              ->where('id', $akun->id)
              ->update(['saldo' => $akun->saldo+$bytunai]);
      }

      if ($bydeposit!=null && $bydeposit>0) {
        DB::table('arus_kas_murid')->insert([
          'akun_id'=>$murid_id,
          'tipe_trx'=>'CREDIT',
          'saldo_awal'=>$saldo,
          'nominal'=>$bydeposit,
          'saldo_akhir'=>$saldo-$bydeposit,
          'no_bukti'=>$no_bukti
        ]);

        DB::table('murid')
              ->where('id', $murid_id)
              ->update(['saldo_deposit' => $saldo-$bydeposit]);
      }

      return redirect('/admin/monitoring-spp');

    }
}
